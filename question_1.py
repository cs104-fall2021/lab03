number1 = float(input("Enter the first number: "))
number2 = float(input("Enter the second number: "))
operation = input("Enter the operation: ")

if operation == "addition":
    print("The result is", number1 + number2)
elif operation == "subtraction":
    print("The result is", number1 - number2)
elif operation == "multiplication":
    print("The result is", number1 * number2)
elif operation == "division":
    print("The result is", number1 / number2)
else:
    print("The operation type is not correct.")
    print("Please use one of \"addition\", \"subtraction\", \"multiplication\", \"division\".")
