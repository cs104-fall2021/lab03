n = 5
for i in range(n - 1, -1, -1):
    for j in range(i):
        print(" ", end="")
    for j in range(2 * n - 1 - i * 2):
        print("*", end="")
    print()

for i in range(n):
    for j in range(i):
        print(" ", end="")
    for j in range(2 * n - 1 - i * 2):
        print("*", end="")
    print()
