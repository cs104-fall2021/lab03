value = 0
for i in range(5):
    for j in range(i + 1):
        value += 1
        print(value, "", end="")
    print()

new_value = 1
previous_value = 0
for i in range(5):
    for j in range(i + 1):
        temporary_value = new_value
        new_value = previous_value + new_value
        previous_value = temporary_value
        print(new_value, "", end="")
    print()
